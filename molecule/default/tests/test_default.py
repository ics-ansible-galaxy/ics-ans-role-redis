import os
import testinfra.utils.ansible_runner


testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_config_file_exists(host):
    f = host.file('/etc/redis.conf')
    assert f.exists


def test_redis_enabled_and_running(host):
    service = host.service('redis')
    assert service.is_running
    assert service.is_enabled
